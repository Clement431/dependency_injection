<?php

include_once 'task.php';

class ToDoList
{

    private array $task;

    public function __construct($task)
    {

        $this->task = $task;
    }

    public function showTask()
    {
        foreach ($this->task as $key => $value) {
            echo $value->getName();
        }
    }

    /**
     * Get the value of task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set the value of task
     *
     * @return  self
     */
    public function setTask($task)
    {
        $this->task = $task;

        return $this;
    }
}
