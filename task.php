<?php

class Task
{

    private string $name;
    private bool $cheked;

    public function __construct(string $name, bool $cheked)
    {
        $this->name = $name;
        $this->cheked = $cheked;
    }


    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of cheked
     */
    public function getCheked()
    {
        return $this->cheked;
    }

    /**
     * Set the value of cheked
     *
     * @return  self
     */
    public function setCheked($cheked)
    {
        $this->cheked = $cheked;

        return $this;
    }
}
